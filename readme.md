# Standalone unsplash slideshow html

A simple html file that loads a random photo from unsplash and refreshes the screen every few minutes

Doubleclick anywhere to switch to full-screen

Licenced under [MIT License](https://opensource.org/licenses/MIT)
